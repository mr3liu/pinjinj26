# Pinjinj26

Pinjinj26 is a modification of pinyin, a Mandarin romanization system. In essence, it uses only the 26 letters of the ISO basic Latin alphabet (a.k.a. the English alphabet). This implies that tones are represented by a means other than diacritics: tone letters.

In consideration of readers who do not like lengthy explanations, I first give instructions on how to try out pinjinj26.

*Note: this document is not fully complete.*

## Usage

### Translators

I have written two sequences of regex replacements which translate pinyin to pinjinj26 and vice versa. The replacements performed are explained further in this document.

When the input is in pinyin format, it should conform to the standards used by Terra Pinyin. This differs from normal pinyin in two ways: tones are marked by a tone number at the end of the word instead of a diacritic on a vowel (with 5 denoting the light tone), and "ü" is replaced by "v". When the input is in pinjinj26 format, it simply follows the standards of pinjinj26. The translation is guaranteed to be accurate when the input is precise, and may also be somewhat reasonable when the input is imprecise. For instance, if tone is omitted during pinyin to pinjinj26 translation, the output will be what is expected (toneless pinjinj26). However, this is not guaranteed in the reverse direction.

The regex replacements are performed by two provided Perl scripts `py_to_pi26` and `pi26_to_py`. Example usage:

```sh
echo 'pin1yin1' >py
echo 'pinjinj' >pi26
perl -p py_to_pi26 <py >pyt
perl -p pi26_to_py <pi26 >pi26t
diff pyt pi26
diff pi26t py
```

### Installation on Rime

Install [Rime](https://rime.im/).

Download `pinjinj26.schema.yaml`.

Place it in the "user folder". Depending on your version of Rime, this is at:
- Weasel: `%APPDATA%\Rime`
- Squirrel: `~/Library/Rime`
- ibus-rime: `~/.config/ibus/rime`
- fcitx-rime: `~/.config/fcitx/rime`
- fcitx5-rime: `~/.local/share/fcitx5/rime/`

Edit `default.custom.yaml` in the user folder: Add `patch/schema_list/schema` with value `pinjinj26` (default schemas should already be listed; follow the same format).

Switch to the Rime input method.

Redeploy. Depending on your version of Rime,
- Weasel: right-click Rime's tray icon and select 重新佈署
- Squirrel: select 重新佈署 from the system language menu
- \*-rime: click the ⟲ (Deploy) button on the input method status bar (or IBus menu)

Switch to the pinjinj26 input schema by pressing F4 or Ctrl+\` and selecting 拼音26.

(Some of these instructions were translated from [the Rime wiki](https://github.com/rime/home/wiki/RimeWithSchemata).)

## Design philosophy

### Goal

In the lens of coding theory, we can view a Mandarin romanization system as a code which maps each distinct valid syllable in the language's inventory (source symbols) to a string of Roman letters (code words). By this definition, and without the use of delimiters (e.g. "'"), pinyin is _not_ a uniquely decodable code. For instance, pinyin "āngé" could be segmented into either the code words "āng" and "é" or "ān" and "gé", which encode the syllables pronounced "肮鹅" and "安革" respectively. Another example, perhaps more familiar, occurs when tonal information is omitted: "Xian" can be segmented into either "xi" and "an" or "xian" ("西安" or "先"). Note that inserting delimiters at segment boundaries eliminates ambiguity, and this is done in the official name of Xi'an.

Also note that this whole issue does not arise in computer implementations of pinyin that represent tones with a word-final tone number instead of diacritics: e.g. "ang1e2" and "Xi1an1" are uniquely decodable. This is because numbers do not occur in the spelling of the initial and final of a syllable (i.e. the syllable minus the tone), so in addition to denoting tone they also act as delimiters. However, a pinyin input engine usually reserves the number keys for selecting the desired Chinese character(s) to be inputted, so in practice most of them do not allow tonal input, and what can be inputted is subject to segmentation ambiguity.

Here I propose an intermediate pinyin modification "dumbpinjinj26" that lies between pinyin and pinjinj26, which does only use the ISO basic Latin alphabet and manages to represent tonal information _and_ is uniquely decodable, but compensates with other downsides; from this we will see why pinjinj26 makes changes to the representation of the initial and final in addition to the tone of a syllable. Note that we will assume that ü and v are equivalent, as is the case for many pinyin input engines. This is the modification: simply choose five letters that only ever appear in the initial of a syllable (this works out to be all letters except those in "aoeiuvng"), and use them as tone letters corresponding to each of the five tones. This forms a prefix-free code (due to the following reasoning), and is thus uniquely decodable.
- x and y are code words.
- x is a proper prefix of y.
- x ends in a (tone) letter l that is initial-only.
- x != y so l is not the last letter in y, so l appears in the initial of y.
- x has a final-only letter before l (no syllable has no empty final).
- y has a final-only letter before its initial. This is impossible.

In essence, the downside of dumbpinjinj26 is that, when used as an input schema, it makes tonal input mandatory. Consider an instance of dumbpinjinj26 which chooses "jqyxw" as the tone letters corresponding to tones 1-5.

...


### Modifications

Pinyin to pinjinj26:

![py_to_pi26.svg](py_to_pi26.svg)

Pinjinj26 to pinyin:

![pi26_to_py.svg](pi26_to_py.svg)

## Miscellaneous notes

The intended typing experience includes 简拼, which I will translate as "initial typing". For example, typing "nh" gives words like "你好", "男孩", and "女孩".

You can use "'" as an explicit delimiter. For example, typing "imi" gives "薏米" and "im'i" gives "有没有" (using initial typing).

The uncommon syllable ê (欸) can be typed with "eh".
